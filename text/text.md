---
title: Declaration for Ethical Apps
subtitle: an attempt at a fundamental rights based proposal
version: "1.0"
---

# DRAFT DRAFT DRAFT

**This is a DRAFT version**

Ethics in an App Manifesto Meeting 03.11.21 last Update dec 3 18:00


# Introduction

Our digital society is heavily dependent on mobile technologies. The majority of citizens use smartphones more frequently than desktop computers or laptops. The mobile first paradigm has increased importance for the past 10+ years. These smartphones are often locked-in computers that heavily limit what users and developers can do on the platform. Apps are the dominant interface that people use to interact with technology. App Stores are often overlooked points of control in our modern digital ecosystems. This Manifesto establishes ethical principles for this ecosystem. For us, the human perspective and fundamental rights are center stage for the design and evaluation of technological systems - following the ideas of 'Digital Humanism'.

This Manifesto is structured according to principles that ethical Apps, App stores and smartphones need to follow. Principles are defined as follows: What is the problem. Why is it a problem. How can it be resolved. Principle to be followed.

# User control over App access on information on the smartphone

Most smartphones contain a wide variety of data points about a persons' life. The photo library on a smartphone can contain thousands of personal photographs. The address book on a smartphone can contain personal information like home address, birthdays, or occupations about third parties who have not given consent for the processing of their information on the smartphone of a third party (while such uses might not fall under the GDPR on a private phone). Music libraries can contain specific information about musical tastes and character trades. Smartphone operating systems need to regulate access to this information with a focus on transparency and user control. Therefore, it shall be a principle of ethical Apps to provide the user with detailed and comprehensible information on about the purpose and consequence of any request for access to data points on the smartphone. Subsequently, it shall be a principle of Ethical smartphone operating systems to provide the user with transparency functions which App has accessed which data points at what time and allow the user to grant or revoke access to these data points. Lastly, it shall be a principle of ethical App stores to prohibit the tieing from consent to access data points on the smartphone from the installation of Apps or from the execution of their primary functions.

#Configureability of smartphones and deletion of all per-installed Apps

An annoying habit of smartphone vendors and carriers is to preinstall devices with Apps that can't be uninstalled by the user. Often these Apps are then also the default for certain functionalities or the configuration of the device limits the user in their capability to change defaults. A good example are Maps and Messaging Apps which the user is often tied to whenever they click on an address or want to send a text message. This problem can be solved if users were given the choice to uninstall all pre-installed Apps and change all defaults about associations with Apps (e.g. address links, telephone links, etc.). Therefore, it shall be a principle of Ethical smartphones to empower users to uninstall all Apps and change all settings.


# Quality labels for Apps in App stores

We acknowledge the enormous bottleneck of App Stores in the distribution of Apps. This necessity can become virtue for the transparency of consumers. Right now, App reviews in App stores are mostly focused on the adherence to terms of services of the store. Information provided is very limited in the usefulness for making informed consumer choices. Establishing third party hallmarks that help identify Apps with a particularly good adherence to high data protection standards or the criteria of this ethical App Manifesto would be as useful as automatically generated labeling of Apps contain tracking and spying functionality by connecting to servers of Facebook, Google, Amazon or other dominant market players.
Therefore, it shall be a principle of ethical App stores to enable third party hallmarks from established consumer protection or data privacy initiatives with the goal of informing consumer decisions about the outstanding positive or negative characteristics of the App.



# Respecting the intent

Big tech has been very successful at evading any regulation of their services. Examples are legal evasion such as hiding disclaimers ("must be older than 13 years for this service") in long EULAs which no one reads (in this example: especially not children). Apart from legal evasion techniques, technical evasion techniques exist which slightly modify a services / app or piece of software to not quite fall under a particular set of regulations (footnote: See the current Haugen vs. Facebook case).

A third form of evasion is the nudging of users to accept certain (un-)privacy settings by repeatedly asking them. Examples: the cookie banners on web sites or asking if a setting should not be turned on every time you open the app. A "no" shall be a "no", the intent of the user must be respected. Dark patterns such as nudging users to accept detrimental privacy settings must be avoided.

Ethical apps and operating systems shall respect the intention of regulations (GDRP, etc.) and the intent of users and not try to circumvent it. Parallel to this, the legal system must adapt to identify the intent of a modification of a service/app or operating system behaviour and compare it with the intent of the regulation it touches.

We already apply this principle in criminal law, where the intent is considered. 



# Simple explanations of the intent and possible consequences

Where an app (or operating system) makes use of a users' data or metadata, the intent of that usage must be explained in simple terms and/or graphically at the level that a child can understand it quickly. Possible consequences of the data being sent to some server for processing must be mentioned. Compare with the cigarette-package warnings.

Users shall be given a choice to disable selective functionalities from apps/operating systems, where specific behaviour is unwanted (and the intent of that shall be respected without further nudging of users. See above).

# Respecting user's time and attention

Often, digitalisation comes with the promise of reducing staff at an organsitation - after all, the main work of filling out forms for a workflow is outsourced to the user. While on the one hand, digitalisation can improve workflows for everyone and thus be a net-gain for both the users and the organisation, often enough it is implemented by simply outsourcing the workload to the users. In the long run, this is disrespectful of user's time and their attention. Users tend to get overloaded by tasks they should now do instead of the clerk who originally would help them in the workflow. The demand therefore must be that whenever workflows are transformed, with possible "easy" app support, great care should be taken to measure if the overall user-experience is quick, efficient and lean. 

# Independent audits

Ethical apps and operating systems shall undergo independent audits, to certify that they conform with the relevant regulations and respect their intent and if the app or operating system follows this manifesto.

Such an audit shall be published in the open, its methodology should be clear and repeatable.



# Disentanglement of smartphones from manufacturer cloud services

Almost all modern smartphones can only be operated in full functionality once a user enters into the terms of services with a proprietary cloud service from the manufacturer. Customers acquire the hardware of their smartphones including the operating system, yet they are barred from fully utilizing it. Untying smartphones from the manufacturers cloud empowers users to have full choice about how they want to use their devices and whom they entrust their data. Therefore, it shall be a principle of ethical smartphones to allow the full functionality of the device (hard- and software) without the necessity of any communication to cloud infrastructure.





# Resetability and security expiration date

Today consumers can buy a new smartphone from the store and within a few months might find themselves in the situation of no longer receiving vital security updates from the manufacturer. While the hardware is still perfectly fine and could be used for many years, the manufacturer might choose to no longer invest in the security maintenance of this particular device. This is both a challenge for the users security, as well as an ecological problem. A solution for this issue that will empower consumers is that smartphones should be sold with a "best before date" for security updates. After this date has expired and the manufacturer chooses not to extend it, they have to release the drivers of all hardware components of the smartphone as free software to allow third party operating systems and community projects to continue sustainable and secure use of the device. Therefore, it shall be a principle of ethical smartphones to be only sold with a clearly communicated date until which the manufacturer is obliged to provide (security) updates for the device and after this date has expired, the manufacturer needs to release the drivers for all hardware components of the smartphone under a free license.





# Alternative App stores & side-loading

Distribution of Apps on Apple iPhone smartphones is only allowed via the Apple App store. This bottleneck severely hinders the choice of users how to use their devices, the innovative capacity of App developers and cements the monopoly position of Apple in the control of 'their' iPhones. A proven solution to this problem is to allow the installation and execution of Apps from App stores of third parties or the installation of Apps via external stor ages or websites (aka: Side-Loading). Therefore, it shall be a principle of Ethical smartphones to allow for third party App stores and side-loading of Apps.




# Limiting user profiling
Many Apps rely on inferences, conclusions or profiling of their users. These predictions about a person are often wrong, can be discriminatory, and can have negative consequences for that person. Such processing of personal identifiable information requires prior informed and active consent under the GDPR. This right is very often violated by Apps and App Stores. Public authorities should refrain completely from profiling of their users by their applications and Apps. Therefore, it shall be a principle of ethical Apps to refrain from user profiling.





# User-control over sensors

Modern smartphones contain a wide variety of sensors which collect information about their users. Without proper safeguards microphones, cameras, location services, movement tracking, etc. are all ubiquitous surveillance technologies in our pockets. The amount of information that can be accumulated about every aspect of a persons' life undermines the essence of our right to privacy and therefore needs to be regulated both in technological and legal measures. Users have to be in control if, when, why and how information from these sensors is accessed by the operating system or Apps. Therefore, it shall be a principle of ethical Apps to obtain informed user consent for a specified purpose, time, and frequency before sensory information shall be assessed. Subsequently, it shall be a principle of Ethical smartphone operating systems to provide users with functionality to obtain knowledge and exercise control over the use of sensory information. The best way to achieve this, is by having hardware switches and control LED's for the sensor equipment on the device. Lastly, it shall be a principle of ethical App stores to prohibit the tieing from consent to access sensory information both from the installation of Apps or from the execution of their primary functions.





# Privacy enhancing technologies for access to personal identifiable information

Given the sheer amount of personal identifiable information that our smartphones process and the ways in which this information can be obtained by third party developers via Apps, it is vital that the smartphone operating system has built in functionality to ensure a responsible -privacy by design- philosophy. Without such design safeguards, consent can become meaningless, because granting access to personal information (sensory data or other data points on the device) will always lead to overly intrusive information obtained by the App. To solve this problem and prevent the erosion of user trust in these systems, users need to have a real choice about the information given to third parties. Therefore, it shall be a principle of Ethical smartphone operating systems to provide for technical safeguards of the principles of privacy by design and by default. In practice, these principles entail that a smartphone operating system has to offer functionality to only give access to address books in a form which technically obfuscates personal identifiable information (e.g., hashed telephone numbers). Access to address books and photo libraries should be fine-granular to only allow an App to access a sub-set of these data points. Consent for obtaining location information of the device needs to be either singular or temporarily restricted, instead of indefinite, and it is also required to be fine granular to allow for fuzzy approximations of detailed locations (e.g. whole districts instead of streets). Subsequently, it shall be a principle of ethical Apps to - following the GDPR – distinguish between personal identifiable information that is required for the primary functionality of the app and ask for further access once freely given informed consent was given by the user.




# Freedom & fairness in payment systems

The internet has produced a completely new level in the freedom of the flow of information. The same freedom is also possible for the flow of money. With the advent of decentralized, protocol based money, the current App store limitations seem unhinged and overly intrusive. App stores should not be allowed to dictate payment processes within the App or in other aspects of the underlying service. Developers should be free to innovate - within existing laws — about the ways in which business models, donation systems and micro credits are offered to users. Therefore, it shall be a principle of ethical App stores to refrain from any restrictions of payment systems, going beyond the price of the software itself or potential in-App purchases via the App store.




# Archivability
smartphones increasingly become our exterior brains and reflect many aspects of our daily lives. The fugacity of this information in App Containers on smartphone Operating systems represents a huge long-term problem. Without the possibility to archive this information users are stripped from control over their information and locked into one particular App or App-Ecosystem. Archivability is already enshrined in the right for data portability under the GDPR and can be implemented without conflicting with principles of data minimization and in commonly accepted formats. Therefore, it shall be a principle of Ethical Apps to ensure the archivability and data export of user data from Apps in practice.

# Planned obsolescence

Smartphones make up a huge amount of electronic waste. The normal life span of a smartphone is / a smarthpone life span is XX years… [RECHERCHE eWaste Anteil von Smartphones, ein paar Zahlen wie viel Müll die Smartphones schon erzeugen]. Done, Petra

Life Span: "The Consumentenbond estimates the average lifespan at 2.5 years. Other sources indicate that a new smartphone will last 15 to 18 months." https://www.coolblue.nl/en/advice/lifespan-smartphone.html

Waste: "Smartphones also contribute to approximately 10% of global e-waste, a number that was estimated to weigh more than 50 million tonnes in 2019[....]the potential value of raw materials in e-waste was valued at USD 57 million in 2019. Meanwhile, recycling rates across electronics stood at only 17% in 2019, meaning the vast majority of this value is not being reaped. [...] 6 of the key elements for mobile phones will run out in the next 100 years." https://www.weforum.org/agenda/2021/07/repair-not-recycle-tackle-ewaste-circular-economy-smartphones/

Hence, it is vital to increase the lifespan of a smartphone and in particular reverse the insentive structure of vendors to plan for obsolescence of their devices in order to sell a new generation of devices. From a privacy perspective it also makes sense to divert from one singular, general device for all aspects of life and move towards several, seperate devices for particular fields or areas of life. Therefore, it shall be a principle of ethical Smartphones to publish information about the resources used in the production of the device, in order to allow for enviromental benchmarking of devices, to publish circuit diagrams, in order to allow for repairability benchmarks of the devices, and to be under liability and the threat of class action suits for enviromental damages, in case the Smartphone is intentionally manufactured or designed in a way as to reduce its lifespan or its repairability.

# Public money, public code, public space

Public authorities increasingly offer Apps to interact with their citizens and provide public service information. Yet, almost all of these Apps are neither open source nor free software. This creates a problem for the transparency of these government functions by a lack of auditing capacity of proprietary software. Terms of Service restrictions can in some cases limit the democratically desired functionality of the software. The future development and distribution of the software is often restricted by licensing regimes that are following the interests of private vendors more than public authorities. Licensing costs are recurring items in the budget, whereas free software is always in the public domain and can be maintained by a multitude of developers without lock-in effects. Similarly, the public spaces for discussion and interaction provided by public authorities should also not be maintained or mediated by private corporations or proprietary software, because terms for citizen interactions should be defined democratically and not by commercial contracts with a vendor.

Therefore, it shall be a principle of ethical Apps from public authorities that they should be under a free and open source license. They should be distributed in AppStores specialised in free software (e.g. F-Droid) and limit the use of proprietary libraries (Google PlayServices).



# Market transparency & no undue interference in AppStores
For the whole of the App ecosystem in modern smartphones the dominant vendors of the devices are also in control of the AppStores and hence they are the gatekeepers for the majority of software developers on these platforms. This form of vertical integration creates a monopoly situation for bringing Services and Apps to the devices. Over the past years we have witnessed the abuse of this power by Apple and Google by copying popular Apps from independend SMEs, using their knowledge from the AppStores to fine-tune their own services and in general tilting the market place in their favour. Meaningful regulation of this field needs to allow for the enforcement of anti-trust rules on AppStores – so as to prevent undue interference in the market place – oblige AppStore providers to offer comprehensive statistical overview about the market from a macroscopic perspective, due process in AppStore review proccess, as well as dispute settlement and legal redress for App vendors, and the possibility of App developers to interact directly with their customers. The Digital Markets Act of the EU and other European legislation are addressing some of these problems.

Therefore, it shall be a principle of ethical AppStores to provide macroscopic transparency about their market places, offer App providers due process for the review of their Apps with the potential of binding dispute resultion mechanisms and legal redress, while also not excluding the possiblity of direct interaction between App providers and their users. Subsequently, existing competition and anti-trust regulation needs to be enforced vigorously via AppStores.


-------------------------------------------------------------------------------------------------------------------------------------------



